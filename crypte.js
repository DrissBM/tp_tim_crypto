var atob = require("atob");
var msgCrypt = "ZWpxZQ==";

//Function de cryptage ----------------------------------------
const calculateHash = (str) => {
  let hash = str
    .split("")
    .map((c, i) => str.charCodeAt(i))
    .map((c) => c + 2)
    .map((c) => String.fromCharCode(c))
    .join("");
  return Buffer.from(hash).toString("base64");
};

//Function de décryptage ----------------------------------------
const decode = (str) => {
  let myvar = atob(str);
  let hash = myvar
    .split("")
    .map((c, i) => myvar.charCodeAt(i))
    .map((c) => c - 2)
    .map((c) => String.fromCharCode(c))
    .join("");
  return hash;
};

//Inutilisable si on doit gérer les espaces comme demandé dans l'énoncé (trouvé sur internet)
const nextLetter = (s) => {
  return s.replace(/([a-zA-Z])[^a-zA-Z]*$/, function (a) {
    var c = a.charCodeAt(0);
    switch (c) {
      case 90:
        return "A";
      case 122:
        return false;
      default:
        return String.fromCharCode(++c);
    }
  });
};

//Méthode brute
const toTest = [
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
  " ",
];
const size = toTest.length;

const decodeBrute = (str) => {
  for (i = 0; i < size; i++) {
    let char1 = toTest[i];
    for (j = 0; j < size; j++) {
      let char2 = toTest[j];
      for (k = 0; k < size; k++) {
        let char3 = toTest[k];
        for (l = 0; l < size; l++) {
          let char4 = toTest[l];
          let word = char1 + char2 + char3 + char4;
          if (calculateHash(word) === str) {
            return word;
          }
        }
      }
    }
  }
  return "ERROR";
};
console.log("Msg recu = ", msgCrypt);
console.log("Msg décodé = ", decode(msgCrypt));
console.log("Msg décodé avec méthode brute = ", decodeBrute(msgCrypt));
